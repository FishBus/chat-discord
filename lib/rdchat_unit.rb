# Copyright 2018 "Kovus" <kovus@soulless.wtf>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# rdchat_unit.rb
#

require "#{__dir__}/unit_manager"
require "#{__dir__}/mqueue_unit"
require "#{__dir__}/stringencode"

class RDChat < MQueueUnit
	def initialize(server)
		super(server)
		puts "Initializing rdchat for #{server.server.id}: #{server}"
		@lastindex = 0
		@techlist = {}
		@entitylist = {}
		@current_players = []
		@current_research = nil
		@bulk_research = []
		get_entities
		get_technologies
		get_current_status
	end
	
	def self.name
		return "rdchat"
	end

	def get_current_status
		@server.rcon.command("rdchat.status") do |packet|
			res = packet.packet.to_s[12..-1].strip.split("\n")
			res.each do |rr|
				begin
					jres = JSON.parse(rr)
					@current_research = jres['current_research']
					@current_players = jres['playerlist']
				rescue JSON::ParserError
					puts "DEBUG: #get_entities: Unable to parse list"
					puts rr
				end
			end
		end
	end
	
	def get_entities
		@server.rcon.command("rdchat.entitylist") do |packet|
			res = packet.packet.to_s[12..-1].strip.split("\n")
			res.each do |rr|
				begin
					data = rr.gsub(/Unknown key: "([a-zA-z0-9\-\.]*)"/, '\1')
					jres = JSON.parse(data)
					@entitylist = jres['entities'].dup
				rescue JSON::ParserError
					puts "DEBUG: #get_entities: Unable to parse list"
					puts rr
				end
			end
		end
	end
	
	def get_technologies
		@server.rcon.command("rdchat.techlist") do |packet|
			res = packet.packet.to_s[12..-1].strip.split("\n")
			res.each do |rr|
				begin
					jres = JSON.parse(rr)
					@techlist = jres['technologies'].dup
				rescue JSON::ParserError
					puts "DEBUG: #get_technologies: Unable to parse tech list"
					puts rr
				end
			end
		end
	end

	def entity_named(name)
		if @entitylist and @entitylist[name]
			return @entitylist[name]
		end
		return name
	end
	
	def entriesProcessed
		# This function is called when all the entries of a batch have been
		# processed, so that if there's any bulk transactions to do, we can
		# do them here.
		p @bulk_research
		completed = @bulk_research.map{|x| x if x[:action] == :finish}.compact
		completed_list = completed.map{|x| x[:name]}
		if completed.size > 0
			msg = "*Research completed: "
			while completed.size > 0
				if msg.size + tech_named(completed.first[:name]).size + 3 > 1500 then
					msg[-1] = "*"
					$bot.send_message(@server.server.channel, msg)
					msg = "*Research completed:"
				end
				res = completed.shift
				lev = " (#{res[:level].to_i})" if res[:level].to_i > 0 and res[:infinite]
				msg += " #{tech_named(res[:name])}#{lev},"
			end
			
			msg[-1] = "*"
			$bot.send_message(@server.server.channel, msg)
		end
		started = @bulk_research.map{|x| x if x[:action] == :start}.compact
		if started.size > 0
			unless completed.include? started.last[:name]
				res = started.last
				msg = "*Research "
				if res[:previous] and res[:previous][:name]
					prev = res[:previous]
					msg += "changed: "
					l1 = " (#{prev[:level].to_i})" if prev[:level].to_i > 0 and prev[:infinite]
					msg += "#{tech_named(prev[:name])}#{l1} -> "
				else
					msg += "started: "
				end
				lev = " (#{res[:level].to_i})" if res[:level].to_i > 0 and res[:infinite]
				msg += "#{tech_named(res[:name])}#{lev}"
				
				msg += "*"
				$bot.send_message(@server.server.channel, msg)
			end
		end
		@bulk_research = []
	end
	
	def tech_named(name)
		if @techlist and @techlist[name]
			return @techlist[name]
		end
		return name
	end
	
	def processEntry(jentry)
		puts "Processing entry #{jentry}"
		res = 0
		if jentry['index'].to_i > @lastindex
			if $bot and @server.server.channel
				data = jentry['message']
				channel = @server.server.channel
				msg = case data['event']
				when 'adminchat'
					channel = @server.server.adminchannel
					"(#{@server.server.name}) <#{data['user']}> #{data['message']}"
				when 'antigrief_alert'
					channel = @server.server.adminchannel
					"*(#{@server.server.name}) antigrief> #{data['message']}*"
				when 'chat'
					"<#{data['user']}> #{data['message']}"
				when 'player_banned'
					if @server.server.adminchannel
						channel = @server.server.adminchannel
						prefix = "(#{@server.server.name}) "
					end
					"*#{prefix.to_s}#{data['source']} has banned <#{data['user']}>: #{data['reason'].to_s}*"
				when 'player_died'
					cause = ''
					if data['cause']
						cause = " by #{entity_named(data['cause'])}"
						cause += (" (#{data['cause_detail'].to_s})" if data['cause_detail']).to_s
					end
					"*Player <#{data['user']}> was killed#{cause}.*"
				when 'player_joined'
					"*Player <#{data['user']}> has joined the game.*"
				when 'player_kicked'
					if @server.server.adminchannel
						channel = @server.server.adminchannel
						prefix = "(#{@server.server.name}) "
					end
					"*#{prefix.to_s}#{data['source']} has kicked <#{data['user']}>: #{data['reason'].to_s}*"
				when 'player_left'
					"*Player <#{data['user']}> has left the game.*"
				when 'research_started'
					@bulk_research.push({
						action: :start,
						name: data['name'],
						infinite: data['infinite'],
						level: data['level'].to_i,
						previous: {
							name: data['last_name'],
							infinite: data['last_infinite'],
							level: data['last_level'],
						},
					})
					nil
				when 'research_finished'
					puts data
					@bulk_research.push({
						action: :finish,
						name: data['name'],
						level: data['level'].to_i,
						infinite: data['infinite'],
					})
					nil
				when 'shout'
					"<#{data['user']} (shout)> #{data['message']}"
				else
					channel = @server.server.adminchannel
					"Unknown event: #{data['event']}.  #{data}"
				end
				
				if msg != nil && channel
					begin
						$bot.send_message(channel, msg[0..500])
						res = msg[0..500].size
					rescue RestClient::GatewayTimeout
						puts "*" * 30
						puts "The API Request to send to discord timed out."
						puts "Messages might be lost, not sure."
						puts "*" * 30
					end
				end
			end
		end
		res
	end
	
	def query_and_process
		#puts "DEBUG: Keystore#query_and_process"
		@server.rcon.command("mqueue.get #{RDChat.name} #{@lastindex}") do |packet|
			#puts "DEBUG: RDChat#query_and_process" # #{packet.packet.to_s[12..-1].strip}"
			highest_index = 0
			highest_tick = 0
			res = packet.packet.to_s[12..-1].strip.split("\n")
			res.each do |rr|
				begin
					jres = DataIO::import(rr)
					next if not jres
					if jres['count'].to_i > 0 and jres['entries'].class == Array
						jres['entries'].sort{|a, b| a['index'].to_i <=> b['index'].to_i}.each do |entry|
							#puts "Evaluating entry #{entry} (lasttick: #{last_tick}) for processing"
							highest_index = [highest_index, entry['index'].to_i].max
							highest_tick = [highest_tick, entry['tick'].to_i].max
							next if entry['tick'].to_i < last_tick
							processEntry(entry)
						end
					end
					puts jres
				rescue JSON::ParserError
					puts "DEBUG: #query_and_process: Unable to parse #{rr}"
				end
			end
			entriesProcessed()
			#puts "DEBUG: Lastindex [#{@lastindex}, #{highest_index}].max ..."
			@lastindex = [@lastindex, highest_index].max
		end
	end
	
	def restart
		@lastindex = 0
		get_entities
		get_technologies
		get_current_status
	end
end

$unitManager.register(RDChat)
