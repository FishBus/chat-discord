# Copyright 2018 "Kovus" <kovus@soulless.wtf>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# UnitManager.rb
# High level class for the units we know about.

# Goal: Preset
# Mostly here for sanity, as I don't want to allow the DataEX to respond to 
# regular ruby methods in addition to the ones we _do_ want to respond to.

class UnitManager
	def initialize
		@@units = {}
	end
	
	def include? unitClass
		return @@units.include? unitClass
	end
	def list
		return @@units.keys
	end
	def register unitClass
		if not unitClass.respond_to? :name
			puts "DEBUG: Attempting to register unit which does not have #name."
			return
		end
		if self.include? unitClass
			puts "Component #{unitClass.name} already registered!"
		else
			# check for a function 'query_and_process'
			if unitClass.public_instance_methods.include? :query_and_process
				@@units[unitClass.name] = unitClass
			else
				puts "Unit #{unitClass.name} does not know #query_and_process."
			end
		end
	end
	def units
		return @@units
	end
end

if not $unitMananger
	$unitManager = UnitManager.new
end
