require 'zlib'

module DataIO
	def self.export(data, method="0")
		if method == "0"
			# data -> (item with tags) -> json -> zlib -> base64
			# note, we have "our" method, then there's the Factorio
			# "blueprintstring import" version (0). => ".00"
			res = JSON.encode({ext: data})
			res = Zlib::Deflate.deflate(res)
			res = ".00#{Base64.encode64(res)}"
			return res
		elsif method == "1"
			# json
			res = JSON.encode(data)
			return res
		end
	end
	
	def self.import_string(data, method)
		if method == "0"
			version = data[0]
			if version == "0"
				# un-base64 -> zlib -> json -> (item with tags) -> data
				res = Base64.decode64(data[1..-1])
				res = Zlib::Inflate.inflate(res)
				res = JSON.parse(res)
				return res["item_with_tags"]["tags"]["ext"]
			else
				puts "ERROR: Unknown blueprint string version: #{version}."
			end
		end
		return nil
	end
	
	def self.import(data)
		if data.size < 2
			p "WARNING: Invalid entry: #{data}"
			return nil
		end
		p data
		marker = data[0]
		if marker == "."
			# convert from known method.
			return import_string(data[2..-1], data[1])
		elsif marker == "{"
			# convert from json
			return JSON.parse(data)
		else
			return import_string(data, "0")
		end
	end
end
