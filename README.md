# RDChat - RCON Discord-Chat for Factorio

RCON-based chat bot for interacting between the Factorio game server & Discord.

### The short story

Adding Discord-Game chat is something that was desired for FishBus Gaming.  There wasn't a management program which we could run on the server to scrape the console, or read exported data to a file.  So we created a RCON-based program to talk to the server.

#### Installation

Installation is two parts.  One part is the [RconChat mod](https://mods.factorio.com/mod/rconchat) for Factorio.  Just install it like any other mod.

The second part in this is in Ruby.  Enter into the folder, and install the gems with bundler.  This chatbot depends on a database.  I use Postgresql, but the ORM used here interacts fairly well with other databases, but YMMV for mysql, mssql, sqlite, etc.

```
bundle install --path .bundle
```


#### Setup & Usage

Copy the `config/defaults/*` files to `config/`, resulting in `config/db.yml` and `config/bot.yml`.  Then edit each of these.

In `config/bot.yml`, you'll want to set your token to the one you get from your Discord app.  Setting up a Discord app/bot is fairly well-documented on https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token .  Read along, and combine the information you learn there to get your token and invite your new bot to your guild/server.

In `config/db.yml`, you'll want to configure the settings for the database you have installed.  None of this code will create your user or database for you, but it will create the tables.

Now that you have your database settings setup, run  `rake migrate` to create the database tables and a sample entry.

Run `rake dbconsole` to connect to the postgresql database (this command expects psql to be installed).  Edit or add a row to the rdchat_servers table to make the bot connect to your server via rcon, and connect to discord for the appropriate channel ID.  (Getting a channel ID is done by right-clicking the channel and choosing "Copy ID".)

Run the bot with 
```
rake run
```
Prefereably inside of a tmux or screen session, so that you can reconnect to it later.  It's sort of verbose, so I do not recommend logging its output.
