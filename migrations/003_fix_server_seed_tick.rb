Class.new(Sequel::Migration) do
	def up
		print "UP Migration: #{__FILE__}\n"
		
		alter_table :rdchat_servers do
			set_column_type  :seed, :Bignum
			set_column_type  :last_tick, :Bignum
		end
	end
	
	def down
		print "DOWN Migration: #{__FILE__}\n"
	end
end
