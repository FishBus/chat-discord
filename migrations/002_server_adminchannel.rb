Class.new(Sequel::Migration) do
	def up
		print "UP Migration: #{__FILE__}\n"
		
		alter_table :rdchat_servers do
			add_column  :adminchannel, String, :size => 50
		end
	end
	
	def down
		print "DOWN Migration: #{__FILE__}\n"
		alter_table :rdchat_servers do
			drop_column :adminchannel
		end
	end
end
