Class.new(Sequel::Migration) do
	def up
		print "UP Migration: #{__FILE__}\n"
		
		create_table :rdchat_servers do
			primary_key	:id
			String      :name, :null => false
			Boolean     :enabled, :default => true
			String      :host, :size => 100, :null => false
			Integer     :rcon_port, :null => false
			String      :rcon_pass, :size => 100, :null => false
			index       [:host, :rcon_port], :unique => true
			String      :channel, :size => 50, :unique => true, :null => false
			
			Integer     :seed
			Integer     :last_tick
		end
	end
	
	def down
		print "DOWN Migration: #{__FILE__}\n"
		drop_table :rdchat_servers
	end
end
